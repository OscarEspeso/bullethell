﻿using UnityEngine;

public class PlayerMover : MonoBehaviour, IMovable {

    [SerializeField]
    private float vel;

	public void move(Vector3 displacement) {
        transform.position += displacement * vel * Time.deltaTime;
    }
}
