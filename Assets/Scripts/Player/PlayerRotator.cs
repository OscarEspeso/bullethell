﻿using UnityEngine;

public class PlayerRotator : MonoBehaviour, IRotatable {
	public void rotate(Vector3 lookPos) {
        transform.LookAt(lookPos);
    }
}
