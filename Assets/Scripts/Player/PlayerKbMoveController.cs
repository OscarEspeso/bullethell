﻿using UnityEngine;

public class PlayerKbMoveController : MonoBehaviour {

    private IMovable mover;
    
	void Start () {
        mover = this.GetComponent<IMovable>();
    }

    void Update () {
    	// Calculate the movement direction from the Unity input axes.
        Vector3 displacement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        // Only move the player if there is an input
        if (displacement != Vector3.zero) {
            mover.move(displacement);
        }
	}
}
