﻿using UnityEngine;

public interface IRotatable {
	/// <summary>
	/// Rotate so the transforms z direction looks towards the specified position.
	/// </summary>
	/// <param name="lookAtDirection">The position to look at.</param>
	void rotate(Vector3 lookAtPosition);
}