﻿using UnityEngine;

public interface IMovable {
	/// <summary>
	/// Add the specified displacement vector to rhe transforms position.
	/// </summary>
	/// <param name="displacement">Displacement vector.</param>
	void move(Vector3 displacement);
}